:- dynamic(vegetable/2).
:- dynamic(transactionInstance/3).
:- dynamic(transaction/5).
:- dynamic(batch/4).
:- dynamic(vegetable/4).
:- dynamic(person/5).
:- dynamic(stock/3).
:- dynamic(enterNext/1).
:- dynamic(recordId/2).
:- dynamic(searchName/1).
:- dynamic(personCategory/2).


% 'id,fee type'
feeType(1,'debt').
feeType(2,'supply').


% 'id,amount,fee type,transaction instance'
fee(1,250,2,1).
fee(2,70,1,1).
fee(3,45,1,2).
fee(4,400,2,2).
fee(5,150,1,3).


% 'id,initial stock unit,current stock unit'
stock(1,200,200).
stock(2,300,300).
stock(3,60,50).
stock(4,30,36).


% 'id,buying price,stock,batch unit'
batch(1,4,1,200).
batch(2,3,2,300).
batch(3,15,3,60).
batch(4,20,4,30).


% 'id,name,selling price,batch'
vegetable(1,'tomato',5,1).
vegetable(2,'onion',5,2).
vegetable(3,'kale',20,3).
vegetable(4,'cabbage',30,4).
% 'had to comment the vegetable records below to allow us to work on faster'
/**
 * vegetable(5,'managu',).
 * vegetable(6,'terere',).
 * vegetable(7,'pepper',).
 * vegetable(8,'garlic',).
 * vegetable(9,'terere',).
 * vegetable(10,'ginger',).
 * vegetable(11,'dhania',).
 */


% 'id,person,date(day,month,year)'
transactionInstance(1,1,date(15,1,2016)).
transactionInstance(2,3,date(3,2,2016)).
transactionInstance(3,5,date(6,3,2016)).
transactionInstance(4,2,date(18,4,2016)).
transactionInstance(5,4,date(12,4,2016)).


% 'id,vegetable,quantity,price,transaction instance'
transaction(1,1,4,20,1). %4*5
transaction(2,2,10,50,2).%10*5 
transaction(3,1,6,30,2). %6*5
transaction(4,2,4,20,3). %4*5
transaction(5,3,1,20,3). %1*20
transaction(6,4,1,30,4). %1*30
transaction(5,4,3,60,4). %3*20
transaction(6,3,3,90,5). %3*30


% 'id,person category'
personCategory(1,'supplier').
personCategory(2,'debtor').


% 'id,first name,last name,phone number,person category'
person(1,'carol','akinyi','0701024798',1).
person(2,'carol','kerubo','0701024798',2).
person(3,'purray','siantayo','0721456789',1).
person(4,'kabuda','kirubi','0717234956',2).
person(5,'wilson','fisk','0716640921',1).


% 'recordName,recordId'
recordId('feeId',5).
recordId('feeTypeId',2).
recordId('personId',5).
recordId('vegetableId',4).
recordId('personCategoryId',2).
recordId('transactionId',6).
recordId('transactionInstanceId',5).
recordId('batchId',4).
recordId('stockId',4).


% 'value'
enterNext(1).

% 'name'
searchName('carol').

% 'function to consult this file'
conv:-
	consult(vegetable). 
	
% 'function to display person if exists'
displayPerson:-
	nl,write('Enter a name: '),read(X),retract(searchName(O)),assertz(searchName(X)),(person(W,X,T,Y,Z),personCategory(Z,C),
	nl,write('The mobile number of '),write(X),write(' '),write(T),write(' is '),write(Y),write(' and is a '),write(C),fail);
	searchName(BB),person(AA,BB,CC,DD,EE)->nl,nl,stop;nl,write('Sorry,that person does not exist in the records').


% 'function to display vegetable if exists'
displayVegetable:-
	nl,write('Enter a vegetable: '),read(Y),vegetable(X,Y,V,W),batch(W,A,B,C),stock(B,D,E),
	nl,write('There are '),write(E),write(' '),write( Y ),E > 1 -> write('s'),write(' in stock.'),!.

% 'function to display person if does not exist'
displayVegetable:-
	nl,write('Sorry,the vegetable is not available').

% 'function to diplay all vegetables'
displayVegetables:-
	nl,write('Id'),format('~11s',["	Vegetable"]),write('     Batch'),write('	Buying Price'),write('	Selling Price'),write('	Stock count'),
	nl,write('``'),format('~11s',["	`````````"]),write('     `````'),write('	````````````'),write('	`````````````'),write('	```````````'),
	loopVeges.

% 'function to loop through all veges,displaying them'
loopVeges:-
	vegetable(A,B,C,D),batch(D,E,F,G),stock(F,H,I),
	nl,write(A),write('	'),format('~15a',[B]),write(D),write('	'),write(E),write('		'),write(C),write('		'),write(I),fail.
	loopVeges.

% 'function to diplay all people'
displayPeople:-
	nl,format('~8s',["Id"]),format('~22s',["Full name"]),format('~15s',["Category"]),format('~15s',["Phone number"]),
	nl,format('~8s',["``"]),format('~22s',["`````````"]),format('~15s',["````````"]),format('~15s',["````````````"]),
	loopPeople.

% 'function to loop through all people,displaying them'
loopPeople:-
	person(A,B,C,D,E),personCategory(E,F),
	nl,format('~d',[A]),write('	'),atom_concat(B,' ',G),atom_concat(G,C,H),format('~22a',[H]),format('~15a',[F]),format('~15a',[D]),fail.
	loopPeople.

 
% 'function to display stock'
displayStock:-
	nl,write('Enter the vegetable name: '),read(A),vegetable(B,A,C,D),batch(D,E,F,G),stock(F,H,I),
	nl,format('~15s',["Vegetable"]),format('~15s',["Initial units"]),format('~15s',["Current units"]),
	nl,format('~15s',["`````````"]),format('~15s',["`````````````"]),format('~15s',["`````````````"]),
	nl,format('~15a',[A]),format('~d',[H]),format('~11s',[" "]),format('~d',[I]),format('~11s',[" "]),!.


% 'function to show a list of suppliers if exists'
displaySuppliers:-
	nl,format('~8s',["Id"]),format('~22s',["Full name"]),format('~15s',["Phone number"]),
	nl,format('~8s',["``"]),format('~22s',["`````````"]),format('~15s',["````````````"]),
	loopSuppliers,!.

% 'function to show a list of suppliers if no supplier exists'
displaySuppliers:-
	write('Sorry! No supplier found').

% 'function to loop through all suppliers displaying them'
loopSuppliers:-
	person(A,B,C,D,E),F='supplier',personCategory(E,F),
	nl,format('~d',[A]),write('	'),atom_concat(B,' ',G),atom_concat(G,C,H),format('~22a',[H]),format('~15a',[D]),fail.
	loopSuppliers.


% 'function to show a list of debtors if exists'
displayDebtors:-
	nl,format('~8s',["Id"]),format('~22s',["Full name"]),format('~15s',["Category"]),format('~17s',["Phone number"]),format('~12s',["Amount"]),format('~15s',["Date"]),
	nl,format('~8s',["``"]),format('~22s',["`````````"]),format('~15s',["````````"]),format('~17s',["````````````"]),format('~12s',["``````"]),format('~15s',["````"]),
	loopDebtors,!.

% 'function to show a list of debtor if no supplier exists'
displayDebtors:-
	write('Sorry! No debtor found').

% 'function to loop through all debtors displaying them'
loopDebtors:-
	PCId is 2,person(PersonId,FName,nlame,MobileNumber,PCId),personCategory(PCId,Category),transactionInstance(TIId,PersonId,date(Day,Month,Year)),transaction(TId,VegId,Quantity,Amount,TIId),
	nl,format('~d',[PersonId]),write('	'),atom_concat(FName,' ',G),atom_concat(G,nlame,Fulnlame),format('~22a',[Fulnlame]),format('~15a',[Category]),format('~15a',[MobileNumber]),format('~2s',[""]),format('~2f',[Amount]),write('	  '),format('~d',[Day]),write('-'),write(Month),write('-'),write(Year),fail.
	loopDebtors.

% 'function to display transactions '
displayTransactions:-
	nl,format('~8s',["Id"]),format('~15s',["Date"]),format('~15s',["Vegetable"]),format('~10s',["Quantity"]),format('~8s',["Price"]),
	nl,format('~8s',["``"]),format('~15s',["````"]),format('~15s',["`````````"]),format('~10s',["````````"]),format('~8s',["`````"]),
	loopTransactions,!.

% 'function to loop through all debtors displaying them'
loopTransactions:-
	transaction(TId,VegId,Quantity,Price,TIId),vegetable(VegId,VegName,SP,Batch),transactionInstance(TIId,PersonId,date(Day,Month,Year)),
	nl,format('~d',[TId]),write('	'),format('~d',[Day]),write('-'),write(Month),write('-'),write(Year),write('	'),format('~15a',[VegName]),format('~d',[Quantity]),format('~2s',[""]),format('\t~2f',[Price]),fail.
	loopTransactions.


 % 'function to display a debtor'
displayDebtor:-
	write('Enter the first name: '),read(FName),person(PersonId,FName,nlame,Mobile,Category),
	nl,write('Debts of '),write(FName ),write(' '),write(nlame),write(' '),write(Mobile),write(':'),
	nl,
	loopInstances(PersonId),!.

% 'function to loop through all debts displaying them'
loopInstances(PersonId):-
	transactionInstance(TIId,PersonId,date(Day,Month,Year)),
	nl,write('	Transaction of date '),format('~d',[Day]),write('-'),write(Month),write('-'),write(Year),write(':'),loopDebts(TIId,PersonId).

% 'function to loop through all debts displaying them'
loopDebts(TIId,PersonId):-
	fee(FeeId,Amount,FeeTypeId,TIId),feeType(FeeTypeId,FeeType),
	nl,write(TIId),write('		'),format('~d',[FeeId]),write('	'),format('~d',[Amount]),write('	'),format('~15a',[FeeType]),fail.
	loopDebts(InstanceId,PersonId).
	loopInstances(PersonId).

% 'function to display debtor if does not exist'
displayDebtor:-
	write('Sorry,that debtor does not exist in the records').


sellVegetable:-
	TIId is 20,FakePersonId is 0,date_time(dt(Y,M,D,_,_,_)),assertz(transactionInstance(TIId,FakePersonId,date(D,M,Y))),loopItems(TIId,0).
		
loopItems(TIId,PreviousTotalCost):-
	nl,write('Enter the name of the vegetable or 0 to exit: '),read(ReadVegName),retract(enterNext(Agree)),assertz(enterNext(ReadVegName)),enterNext(Value),number(Value)->(
	PreviousTotalCost < 1 -> (nl,write('No sale made'),nl,nl,stop);(
	write('Enter the cash  given:	'),read(Cash),
	(Cash < PreviousTotalCost -> 
		Debt is PreviousTotalCost - Cash,nl,write('The debt is: '),write(Debt),
		nl,nl,write('Provide debtor''s details below'),
		nl,write('```````````````````````````````'),	
		Category is 2,
		nl,write('Enter first name: '),read(FName),
		write('Enter last name: '),read(nlame),
		write('Enter mobile num with single quotes'),
		nl,write(' e.g. ''0701234567'''),write(': '),read_atom(Mobile),
		
		retract(transactionInstance(TIId,PersonId,date(D,M,Y))),		
		(person(PersonId,FName,nlame,Mobile,Category),assertz(transactionInstance(TIId,PersonId,date(D,M,Y)))
		;NewPersonId is 21,assertz(person(NewPersonId,FName,nlame,Mobile,Category)),assertz(transactionInstance(TIId,NewPersonId,date(D,M,Y))))
	;Change is Cash - PreviousTotalCost,nl,write('The change to be issued is: '),write(Change),nl),		
	
	nl,write('Sale made successfully.'),nl,nl,stop))
	
	;(enterNext(VegName),vegetable(VegId,VegName,SP,BatchId),batch(BatchId,BP,StockId,BatchUnit),stock(StockId,ISU,CSU),
	write('Enter the quantity of the vegetable: '),read(Quantity),

	VegetableCost is SP * Quantity,NewTotalCost is PreviousTotalCost + VegetableCost,assertz(transaction(10,VegId,Quantity,TotalCost,TIId)),

	nl,write('Vegetable  : '),write(VegName),
	nl,write(VegName),write(' cost: '),write(VegetableCost),	
	
	NewBatchUnit is BatchUnit - Quantity,
	retract(batch(BatchId,BP,StockId,BatchUnit)),
	assertz(batch(BatchId,BP,StockId,NewBatchUnit)),

	NewCSU is CSU - Quantity,
	retract(stock(StockId,ISU,CSU)),
	assertz(stock(StockId,ISU,NewCSU)),	

	nl,write('Total cost: '),write(NewTotalCost),
	%nl,write('Enter nex?(y/n): '),read(Continue),Continue = (),	
	nl,loopItems(TIId,NewTotalCost)).
				

% 'function to remove a vegetable,probably when the seller ceases to sell this vegetable'
deleteVegetable:-
	nl,write('Enter the name of the vegetable to remove permanently: '),read(VegName),
	nl,retract(vegetable(VegId,VegName,SP,BatchId)),
	write('Vegetable removed successfully.'),!.

% 'function to inform of failure in deletion of a vegetable'
deleteVegetable:-
	write('We could not find that vegetable record').


% 'function to delete a person record'	
deletePerson:-
	nl,write('Enter the first name of the person to remove permanently: '),read(FName),
	nl,retract(person(PersonId,FName,nlame,PhoneNo,PCId)),
	write('Person removed successfully'),!.

% 'function to inform of failure in deletion of a person'
deletePerson:-
	write('We could not find that person record').


% 'function to change the price of the vegetable'
updatePrice:-
	displayVegetables,
	nl,nl,write('Enter the name of the vegetable: '),read(Y),vegetable(X,Y,V,W),batch(W,A,B,C),stock(B,D,E),
	retract(vegetable(X,Y,V,W)),
	nl,write('Enter the new batch quantity: '),read(NC),
	write('Enter the batch buying price: '),read(NA),
	write('Enter the new selling price: '),read(NV),nl,
	NW is 21,ND is D + NC,NewE is E + NC,NB is B,
	retract(stock(B,D,E)),
	assertz(stock(NB,ND,NewE)),
	assertz(batch(NW,NA,B,NC)),
	assertz(vegetable(X,Y,NV,NW)),
	write('New batch recorded and price changed successfully'),
	nl,displayVegetables,!.


% 'function to add a new batch'	
addBatch:-
% 'Update IDs'	
	
	recordId('stockId',StockId),recordId('batchId',BatchId),recordId('vegetableId',VegId),recordId('transactionInstanceId',TIId),recordId('transactionId',TId),
	
	NewStockId is StockId + 1,
	retract(recordId('stockId',StockId)),
	assertz(recordId('stockId',NewStockId)),

	NewBatchId is BatchId + 1,
	retract(recordId('batchId',BatchId)),
	assertz(recordId('batchId',NewBatchId)),
	
	NewVegId is VegId + 1,
	retract(recordId('vegetableId',VegId)),
	assertz(recordId('vegetableId',NewVegId)),

	NewTId is TId + 1,
	retract(recordId('transactionId',TId)),
	assertz(recordId('transactionId',NewTId)),
	
	NewTIId is TIId + 1,
	retract(recordId('transactionInstanceId',TIId)),
	assertz(recordId('transactionInstanceId',NewTIId)),
	date_time(dt(Y,M,D,H,Mn,S)),

%'Let the vegetable owner choose the type of supplier'
	nl,write('1. Regular Supplier.'),
	nl,write('2. New Supplier.'),
	nl,nl,write('Enter the type of supplier: '),
	read(X),

%'It is a cash transaction  i.e the transaction instance is set to zero and no detail for the person recorded.'
	(X=:=1 ->
	nl,write('THE FOLLOWING ARE YOUR REGULAR SUPPLIERS:'),
	nl,displaySuppliers,
	
	nl,nl,write('Enter the ID of the supplier: '),read(PersonId),
	person(PersonId,FName,nlame,Mobile,PCId),personCategory(PCId,PC),

% 'Capture the details of the batch'
	nl,write('CAPTURE DETAILS OF THE BATCH'),
	nl,write('````````````````````````````'),nl,
	write('Vegetable name: '),read(VegName),
	write('Vegetable quantity: '),read(Quantity),
	write('Buying price(per unit): '),read(BP),
	write('Proposed selling price(per unit): '),read(SP),

	CoG is BP * Quantity,	
	nl,write('The cost of the goods is: '),write(CoG),
	nl,write('Enter the cash given to '),write(FName),write(': '),read(Cash),	
	Change is Cash - CoG,	
	nl,write('The change to receive is: '),write(Change),

% 'Insert into the databases'
	assertz(batch(NewBatchId,BP,NewStockId,Quantity)),	
		(vegetable(VId,VegName,OldSP,BId) -> (batch(BId,OldBP,SId,Q),stock(SId,ISU,CSU),
		NewISU is ISU + Quantity, NewCSU is CSU + Quantity,
		retract(stock(SId,ISU,CSU)),assertz(stock(NewStockId,NewISU,NewCSU)),
		retract(batch(BId,OldBP,SId,Q)),assertz(batch(BId,OldBP,NewStockId,Q)),
		retract(vegetable(VId,VegName,OldSP,BId)),
		assertz(vegetable(VId,VegName,SP,BId)))
	;
		assertz(stock(NewStockId,Quantity,Quantity)),
		assertz(vegetable(NewVegId,VegName,SP,NewBatchId))),
	assertz(transactionInstance(NewTIId,PersonId,date(D,M,Y))),
	assertz(transaction(NewTId,NewVegId,Quantity,CoG,NewTIId)),
	
	nl,nl,write('Batch added successfully.'),nl,nl,
	write('SUPPLIES RECORD: '),nl,
	displaySupplies
	;
% 'Update Person Id key'
	recordId('personId',PI),
	NewPI is PI + 1,
	retract(recordId('personId',PI)),
	assertz(recordId('personId',NewPI)),

	
% 'Capture the details of the batch'
	nl,write('CAPTURE DETAILS OF THE BATCH'),
	nl,write('````````````````````````````'),nl,
	write('Vegetable name: '),read(VegName),
	write('Vegetable quantity: '),read(NewQuantity),
	write('Buying price(per unit): '),read(NewBP),
	write('Proposed selling price(per unit): '),read(NewSP),

	NewCoG is NewBP * NewQuantity,	
	nl,write('The cost of the goods is: '),write(NewCoG),
	nl,write('Enter the cash given: '),read(Cash),
	
	Change is Cash - NewCoG,	
	nl,write('The change to receive is: '),write(Change),nl,

% 'Capture the details of the supplier first'
	nl,write('CAPTURE DETAILS OF THE SUPPLIER'),
	nl,write('```````````````````````````````'),nl,
	write('First name: '),read(FN),
	write('Last name: '),read(nl),
	write('Mobile number with single quotes e.g. ''0701234387'' : '),read_atom(MN),

% 'Insert  into the databases'
	PC is 1,
	assertz(person(NewPI,FN,nl,MN,PC)),
	assertz(batch(NewBatchId,BP,NewStockId,Quantity)),	
		(vegetable(VId,VegName,OldSP,BId) -> (batch(BId,OldBP,SId,Q),stock(SId,ISU,CSU),
		NewISU is ISU + NewQuantity, NewCSU is CSU + NewQuantity,
		retract(stock(SId,ISU,CSU)),assertz(stock(NewStockId,NewISU,NewCSU)),
		retract(batch(BId,OldBP,SId,Q)),assertz(batch(BId,OldBP,NewStockId,Q)),
		retract(vegetable(VId,VegName,OldSP,BId)),
		assertz(vegetable(VId,VegName,NewSP,BId)))
	;
		assertz(stock(NewStockId,NewQuantity,NewQuantity)),
		assertz(vegetable(NewVegId,VegName,NewSP,NewBatchId))),
	assertz(transactionInstance(NewTIId,NewPI,date(D,M,Y))),
	assertz(transaction(NewTId,NewVegId,NewQuantity,NewCoG,NewTIId)),
		
	nl,write('Batch added successfully.'),nl,nl,
	write('SUPPLIES RECORD: '),nl,
	displaySupplies	
	).

%'Function to display all the available debts'   
displaySupplies:-
	nl,format('~22s',["Full name"]),format('~17s',["Phone number"]),format('~15s',["Vegetable"]),format('~12s',["Quantity"]),format('~15s',["Date"]),
	nl,format('~22s',["`````````"]),format('~17s',["````````````"]),format('~15s',["`````````"]),format('~12s',["````````"]),format('~15s',["````"]),
	loopSupplies,!.
	
% 'function to show a list of supplies if none exists'
displaySupplies:-
	write('Sorry! No supply found').
	
% 'function to loop through all veges,displaying them'
loopSupplies:-
	PCId is 1,person(PersonId,FName,nlame,MobileNumber,PCId),transactionInstance(TIId,PersonId,date(Day,Month,Year)),transaction(TId,VegId,Quantity,I,TIId),vegetable(VegId,VegName,SP,BatchId),
	nl,atom_concat(FName,' ',Temp),atom_concat(Temp,nlame,Fulnlame),format('~22a',[Fulnlame]),format('~15a',[MobileNumber]),format('~15a',[VegName]),format('~4s',[""]),format('~d',[Quantity]),write('	  '),format('~d',[Day]),write('-'),write(Month),write('-'),write(Year),fail.
	loopSupplies.


% 'function to display record ids'
displayId:-
	nl,recordId(Record,Id),write('Record: '),format('~25a',[Record]),write(' -> Id: '),write(Id),nl,fail.
	displayId.

% 'function to display all stock records'
displayStocks:-
	nl,write('Id'),format('~25s',["	Initial stock unit"]),format('~23s',["Current stock unit"]),format('~11s',["Vegetable"]),
	nl,write('``'),format('~25s',["	``````````````````"]),format('~23s',["``````````````````"]),format('~11s',["`````````"]),
	loopStocks.

% 'function to loop through all stock records,displaying them'
loopStocks:-
	stock(F,H,I),batch(D,E,F,G),vegetable(A,B,C,D),
	nl,write(F),write('	'),format('~d',[H]),write('\t'),format('~16s',[""]),format('~d',[I]),format('~10s',[""]),write('		'),write(B),fail.
	loopStocks.
	
	
% 'function to delete a stock record'	
deleteStock:-
	displayStocks,
	nl,nl,write('Enter the ID of the stock record to remove permanently: '),read(SId),
	nl,retract(stock(SId,InitialSU,CurrentSU)),
	write('stock record removed successfully'),!.

% 'function to inform of failure in deletion of a stock record'
deleteStock:-
	nl,nl,write('We could not find that stock record').


% 'function to display all stock records'
displayBatches:-
	nl,write('Id'),format('~18s',["	Buying price"]),format('~15s',["Batch unit"]),format('~11s',["Vegetable"]),
	nl,write('``'),format('~18s',["	````````````"]),format('~15s',["``````````"]),format('~11s',["`````````"]),
	loopBatches.

% 'function to loop through all stock records,displaying them'
loopBatches:-
	batch(D,E,F,G),vegetable(A,B,C,D),
	nl,write(D),write('	'),format('~d',[E]),write('\t'),format('~9s',[""]),format('~d',[F]),format('~12s',[""]),write('	'),write(B),fail.
	loopBatches.
	

% 'function to a delete batch record'	
deleteBatch:-
	displayVegetables,nl,displayBatches,
	nl,nl,write('Enter the ID of the batch record to remove permanently: '),read(BId),
	nl,retract(batch(BId,BP,Stock,BatchU)),
	write('Batch record removed successfully'),nl,displayBatches,!.

% 'function to inform of failure in deletion of a batch record'
deleteBatch:-
	nl,nl,write('We could not find that batch record').


% ' functiont to add a person category record'
addPersonCategory:-
	recordId('personCategoryId',PCId),
	
	NewPCId is PCId + 1,
	retract(recordId('personCategoryId',PCId)),
	assertz(recordId('personCategoryId',NewPCId)),
	
% 'Get the details of the category' 
	nl,write('DETAILS OF THE CATEGORY'),
	nl,write('````````````````````````'),nl,
	write('Enter category name: '),read(Cat),
	
% 'Insert  into the databases'
	assertz(personCategory(NewPCId,Cat)),	
	PC is NewPCId, 
	nl,write('Person Category added Successfully'),
	displayPersonCategories.


%' function to display all person categories'
displayPersonCategories:-
	nl,nl,format('~5s',["Id"]),format('~20s',["Person category"]),
	nl,format('~5s',["``"]),format('~20s',["```````````````"]),
	loopPersonCategories.

% 'function to loop through all person categories,displaying them'
loopPersonCategories:-
	personCategory(Id,PC),
	nl,write(Id),format('~4s',[""]),format('~20a',[PC]),fail.
	loopPersonCategories.
	
	
% 'function to change person category of a person'
changePersonCategory:-
	displayPeople,
	nl,nl,write('Enter the id of the person to change category: '),
	read(PId),person(PId,FN,LN,PN,PCId),
	retract(person(PId,FN,LN,PN,PCId)),
	displayPersonCategories,
	nl,nl,write('Enter the new category id: '),read(NPC),
	assertz(person(PId,FN,LN,PN,NPC)),
	nl,write('Person category changed successfully'),
	nl,displayPeople,!.
	
changePersonCategory:-
	nl,write('Could not change the category').


% 'function to rename a category'
renamePersonCategory:-
	displayPersonCategories,
	nl,nl,write('Enter id of category to rename: '),read(PCId),
	nl,write('Enter the new category name: '),read(NPC),
	retract(personCategory(PCId,PC)),
	assertz(personCategory(PCId,NPC)),
	nl,write('Person category changed successfully'),
	nl,displayPersonCategories,!.
	
	
	
	
	
