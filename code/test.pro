

cont:-
	consult(test). 
 
:-foo(4,2).
  
veg:-
	write('Vegetable name: '),read(VegName),
	vegetable(VId,VegName,OldSP,BId),write('	'),write(VegName),write(' '),write(OldSP),nl,fail.
	veg.
	
read:-
	read_atom(W),write(W),read(U),write(U).
	
date:-
	date_time(dt(Y,M,D,H,Mn,S)), write(Y),write('-'),write(M),write('-'),write(D).
	    	
check:-
	PersonId is 10, var(PersonId) -> nl, stop ; write(PersonId).


sell:-
	TIId is 20,FakePersonId is 0,date_time(dt(Y,M,D,_,_,_)), assertz(transactionInstance(TIId,FakePersonId,date(D,M,Y))),loopItems(TIId,0).
	
		
loopItems(TIId,PreviousTotalCost):-
	nl,write('Enter the name of the vegetable: '),read(ReadVegName),retract(enterNext(Agree)),assertz(enterNext(ReadVegName)),enterNext(Value),number(Value)->(
	PreviousTotalCost < 1 -> (nl,write('No sale made'),nl,nl,stop);(
	write('Enter the cash  given:	'),read(Cash),
	(Cash < PreviousTotalCost -> 
		Debt is PreviousTotalCost - Cash,nl,write('The debt is: '),write(Debt),
		nl,nl,write('Provide debtor''s details below'),
		nl,write('```````````````````````````````'),	
		Category is 2,
		nl,write('Enter first name: '),read(FName),
		write('Enter last name: '),read(LName),
		write('Enter mobile num with single quotes'),
		nl,write(' e.g. ''0701234567'''),write(': '),read_atom(Mobile),
		
		retract(transactionInstance(TIId,PersonId,date(D,M,Y))),		
		(person(PersonId,FName,LName,Mobile,Category),assertz(transactionInstance(TIId,PersonId,date(D,M,Y)))
		;NewPersonId is 21,assertz(person(NewPersonId,FName,LName,Mobile,Category)),assertz(transactionInstance(TIId,NewPersonId,date(D,M,Y))))
	;Change is Cash - PreviousTotalCost,nl,write('The change to be issued is: '),write(Change)),		
	
	nl,nl,write('Sale made successfully.'),nl,nl,stop))
	
	;(enterNext(VegName),write(VegName),vegetable(VegId,VegName,SP,BatchId),batch(BatchId,BP,StockId,BatchUnit),stock(StockId,ISU,CSU),
	write('Enter the quantity of the vegetable: '),read(Quantity),

	VegetableCost is SP * Quantity, NewTotalCost is PreviousTotalCost + VegetableCost,assertz(transaction(10,VegId,Quantity,TotalCost,TIId)),

	nl,write('Vegetable  : '),write(VegName),
	nl,write(VegName),write(' cost: '),write(VegetableCost),	
	
	NewBatchUnit is BatchUnit - Quantity,
	retract(batch(BatchId,BP,StockId,BatchUnit)),
	assertz(batch(BatchId,BP,StockId,NewBatchUnit)),

	NewCSU is CSU - Quantity,
	retract(stock(StockId,ISU,CSU)),
	assertz(stock(StockId,ISU,NewCSU)),	

	nl,write('Total cost: '),write(NewTotalCost),
	%nl,write('Enter nex?(y/n): '),read(Continue),Continue = (),	
	nl,loopItems(TIId,NewTotalCost)).


