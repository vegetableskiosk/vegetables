:- dynamic(vegetable/2).
:- dynamic(transactionInstance/3).
:- dynamic(transaction/5).
:- dynamic(batch/4).
:- dynamic(vegetable/4).
:- dynamic(person/5).
:- dynamic(stock/3).
:- dynamic(enterNext/1).
:- dynamic(recordId/2).
:- dynamic(searchName/1).


% 'id,fee type'
feeType(1,'debt').
feeType(2,'supply').


% 'id,amount,fee type,transaction instance'
fee(1,250,2,1).
fee(2,70,1,1).
fee(3,45,1,2).
fee(4,400,2,2).
fee(5,150,1,3).


% 'id,initial stock unit,current stock unit'
stock(1,200,200).
stock(2,300,300).
stock(3,60,50).
stock(4,30,36).


% 'id,buying price,stock,batch unit'
batch(1,4,1,200).
batch(2,3,2,300).
batch(3,15,3,60).
batch(4,20,4,30).


% 'id,name,selling price,batch'
vegetable(1,'tomato',5,1).
vegetable(2,'onion',5,2).
vegetable(3,'kale',20,3).
vegetable(4,'cabbage',30,4).
% 'had to comment the vegetable records below to allow us to work on faster'
/**
 * vegetable(5,'managu',).
 * vegetable(6,'terere',).
 * vegetable(7,'pepper',).
 * vegetable(8,'garlic',).
 * vegetable(9,'terere',).
 * vegetable(10,'ginger',).
 * vegetable(11,'dhania',).
 */


% 'id,person,date(day,month,year)'
transactionInstance(1,1,date(15,1,2016)).
transactionInstance(2,3,date(3,2,2016)).
transactionInstance(3,5,date(6,3,2016)).
transactionInstance(4,2,date(18,4,2016)).
transactionInstance(5,4,date(12,4,2016)).


% 'id,vegetable,quantity,price,transaction instance'
transaction(1,1,4,20,1). %4*5
transaction(2,2,10,50,2).%10*5 
transaction(3,1,6,30,2). %6*5
transaction(4,2,4,20,3). %4*5
transaction(5,3,1,20,3). %1*20
transaction(6,4,1,30,4). %1*30
transaction(5,4,3,60,4). %3*20
transaction(6,3,3,90,5). %3*30


% 'id,person category'
personCategory(1,'supplier').
personCategory(2,'debtor').


% 'id,first name,last name,phone number,person category'
person(1,'carol','akinyi','0701024798',1).
person(2,'carol','kerubo','0701024798',2).
person(3,'purray','siantayo','0721456789',1).
person(4,'kabuda','kirubi','0717234956',2).
person(5,'wilson','fisk','0716640921',1).


% 'recordName,recordId'
recordId('feeId',5).
recordId('feeTypeId',2).
recordId('personId',5).
recordId('vegetableId',4).
recordId('personCategoryId',2).
recordId('transactionId',6).
recordId('transactionInstanceId',5).
recordId('batchId',4).
recordId('stockId',4).


% 'value'
enterNext(1).

% 'name'
searchName('carol').

